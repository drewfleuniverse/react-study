require("./index.css")

import React from 'react'
import { render } from 'react-dom'
import { Router, Route, browserHistory, IndexRoute } from 'react-router'
import Home from './modules/Home.jsx'
import MainLayout from './modules/MainLayout.jsx'
import SearchLayout from './modules/SearchLayout.jsx'
import UserList from './modules/UserList.jsx'
import UserProfile from './modules/UserProfile.jsx'
import WidgetList from './modules/WidgetList.jsx'

render((
  <Router history={browserHistory}>
    <Route component={MainLayout}>
      <Route path="/" component={Home} />
      <Route path="users">
        <Route component={SearchLayout}>
          <IndexRoute component={UserList} />
        </Route>
        <Route path=":userId" component={UserProfile} />
      </Route>
      <Route path="widgets">
        <Route component={SearchLayout}>
          <IndexRoute component={WidgetList} />
        </Route>
      </Route>
    </Route>
  </Router>
), document.getElementById('root'))
