const webpack = require('webpack');
const path = require('path');

var APP_DIR = path.resolve(__dirname, 'app');
var BUILD_DIR = path.resolve(__dirname, 'public');

var config = {
  entry: APP_DIR + '/index.jsx',
  output: {
    filename: 'bundle.js',
    publicPath: '',
    path: BUILD_DIR
  },
  module: {
    loaders: [
      { test: /\.jsx?/, include: APP_DIR, loader: 'babel-loader?presets[]=es2015&presets[]=react'},
      { test: /\.css$/, include: APP_DIR, loader: "style!css" }
    ]
  },
  plugins: [
    new webpack.DefinePlugin({
      'process.env':{
        NODE_ENV: JSON.stringify('development')
      }
    }),
    new webpack.ProvidePlugin({
      React: "react"
    }),
    new webpack.optimize.UglifyJsPlugin({
      cacheFolder:BUILD_DIR + '/cached_uglify/',
      debug: true,
      minimize: true,
      sourceMap: true,
      output: {
        comments: false
      },
      compressor: {
        warnings: false
      }
    })
  ]
};

module.exports = config;
