I love Andrew~~ Andrew loves me~~ We have 3 children, they are Peter, Cuny, and Ruby.
Peter wants a girlfriend, but he has no dingding...
Cuny is a good girl, never complain and never trouble other
Ruby is a lovely baby, likes hanging out with Peter, big brother, and enjoy time with Cuny, nice sister, and being cute to mommy and daddy.
/**
 * This is just a dummy server to facilidate our React SPA examples.
 * For a more professional setup of Express, see...
 * http://expressjs.com/en/starter/generator.html
 */

import express from 'express';
import path from 'path';
const app = express();


/**
 * Anything in public can be accessed statically without
 * this express router getting involved
 */

app.use(express.static(path.join(__dirname, 'public'), {
  dotfiles: 'ignore',
  index: false
}));


/**
 * Always serve the same HTML file for all requests
 */

app.get('*', function(req, res, next) {
  console.log('Request: [GET]', req.originalUrl)
  res.sendFile(path.resolve(__dirname, 'index.html'));
});


/**
 * Error Handling
 */

app.use(function(req, res, next) {
  console.log('404')
  let err = new Error('Not Found');
  err.status = 404;
  next(err);
});

app.use(function(err, req, res, next) {
  res.sendStatus(err.status || 500);
});


/**
 * Start Server
 */

const port = 3000;
app.listen(port);

console.log('Serving: localhost:' + port);
