import React from 'react';
import {render} from 'react-dom';
import CommentBox from './CommentBox/CommentBox.jsx';

render(
  <CommentBox url="http://localhost:3000/comments" pollInterval={2000} />,
  document.getElementById('content')
);
