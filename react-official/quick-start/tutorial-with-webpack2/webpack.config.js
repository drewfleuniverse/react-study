/*
  Consider the following plugins, and more may be found in the future.
  - webpack-s3-plugin
  - html-webpack-plugin
  - ProvidePlugin
  - ImageminPlugin

  [ASSET HASHING WITH WEBPACK](https://webpack.github.io/docs/long-term-caching.html)
*/

const webpack = require('webpack');
const path = require('path');

var APP_DIR = path.resolve(__dirname, 'src/client/app');
var BUILD_DIR = path.resolve(__dirname, 'src/client/public');

var config = {
  entry: APP_DIR + '/index.jsx',
  output: {
    path: BUILD_DIR,
    filename: 'index.bundle.js'
  },
  module: {
    loaders: [
      { test: /\.jsx?/, include: APP_DIR, loader: 'babel'},
      { test: /\.css$/, include: APP_DIR, loader: "style!css" }
    ]
  },
  plugins: [
    new webpack.DefinePlugin({
      'process.env':{
        NODE_ENV: JSON.stringify('development')
      }
    }),
    new webpack.ProvidePlugin({
      React: "react"
    }),
    new webpack.optimize.UglifyJsPlugin({
      cacheFolder:BUILD_DIR + '/cached_uglify/',
      debug: true,
      minimize: true,
      sourceMap: true,
      output: {
        comments: false
      },
      compressor: {
        warnings: false
      }
    })
  ]
};

module.exports = config;
