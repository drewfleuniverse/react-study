React Auth
- [ReactTraining/react-router/examples/auth-flow/](https://github.com/ReactTraining/react-router/tree/master/examples/auth-flow)

React Goodies
- [Redux Form](http://redux-form.com/6.0.1/)
  - [insin/AddTravel.js](https://gist.github.com/insin/bbf116e8ea10ef38447b)
    - Use redux-form, react-bootstrap, and react-widgets
- react-widgets, use for time picker
- react-chart
- react-modal

Redux Goodies
- [immutable.js](https://facebook.github.io/immutable-js/)
- [gajus/redux-immutable](https://github.com/gajus/redux-immutable)
- [flow](https://flowtype.org/)
- [reactjs/reselect](https://github.com/reactjs/reselect)
- [Redux Dev Tool](https://github.com/gaearon/redux-devtools/blob/master/docs/Walkthrough.md)
- [acdlite/redux-actions](https://github.com/acdlite/redux-actions)
- [paularmstrong/normalizr](https://github.com/paularmstrong/normalizr)
- [gaearon/redux-thunk](https://github.com/gaearon/redux-thunk)

React Router Goodies
- [reactjs/react-router-redux](https://github.com/reactjs/react-router-redux)
- [ReactTraining/react-history](https://github.com/ReactTraining/react-history)

Utility Goodies
- [isomorphic-fetch](https://github.com/matthew-andrews/isomorphic-fetch)
  - Good at client- and server-side rendering, i.e. Isomorphic!
- [SuperAgent, ajax](https://visionmedia.github.io/superagent/)
- [mzabriskie/axios, ajax](https://github.com/mzabriskie/axios)
 - Better than fetch, needs to compare with isomorphic-fetch
- lodash
- ramda

Other Readings
- [Using NGINX on Heroku to Serve Single Page Apps and Avoid CORS](https://m.alphasights.com/using-nginx-on-heroku-to-serve-single-page-apps-and-avoid-cors-5d013b171a45#.2ith7pivw)

React Readings
- [React Community](https://github.com/reactjs)

Redux Readings
- [Action Strategies](https://github.com/bradwestfall/CSS-Tricks-React-Series/blob/master/guide-3-redux/docs/action-strategies.md)
- [A Beginner's Guide to Redux Middleware](https://www.codementor.io/reactjs/tutorial/beginner-s-guide-to-redux-middleware)

React Router Readings
- [Leveling Up With React: React Router](https://css-tricks.com/learning-react-router/)

Other Readings
- [The Six Things You Need To Know About Babel 6](http://jamesknelson.com/the-six-things-you-need-to-know-about-babel-6/)
- [At Facebook, we use Jest to test React applications](https://facebook.github.io/jest/docs/tutorial-react.html)
- [How to Implement Node + React Isomorphic JavaScript & Why it Matters](https://strongloop.com/strongblog/node-js-react-isomorphic-javascript-why-it-matters/)
- [Immutability in JavaScript](https://www.sitepoint.com/immutability-javascript/)

```js
npm i -S react \
  react-dom
  react-redux
  react-router-redux
  redux
  immutable

//  Other packages
babel-cli
babel-core
babel-polyfill

webpack-dev-server
```
